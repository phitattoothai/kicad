
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x03, 0x15, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xad, 0x95, 0x5b, 0x48, 0x93,
 0x61, 0x18, 0xc7, 0x47, 0x74, 0x19, 0x74, 0xd9, 0x55, 0xb7, 0xd5, 0x45, 0x07, 0xc2, 0x79, 0x02,
 0x91, 0x76, 0xb0, 0x6d, 0x2e, 0x0d, 0x75, 0x39, 0xa7, 0xce, 0x63, 0xa4, 0x5d, 0xda, 0xd1, 0xa4,
 0x83, 0x60, 0x08, 0xae, 0xc4, 0xb0, 0x0c, 0xec, 0x22, 0xac, 0xc4, 0x50, 0x67, 0xb1, 0x28, 0x34,
 0xb0, 0x74, 0xea, 0xc6, 0x96, 0x87, 0x4d, 0x97, 0x9a, 0xc7, 0x34, 0x2d, 0x3b, 0x79, 0x98, 0x3a,
 0xb7, 0x99, 0xee, 0xdf, 0xf7, 0xbd, 0x6e, 0x63, 0xd3, 0x1d, 0xa9, 0x07, 0x7e, 0x0c, 0xbe, 0x7d,
 0xef, 0xf3, 0x7b, 0x9f, 0xe7, 0xfd, 0xde, 0xf7, 0x65, 0x30, 0xa8, 0xd0, 0xab, 0x1b, 0x4f, 0xe9,
 0x55, 0xf2, 0x0f, 0x14, 0x56, 0x0a, 0x6c, 0x27, 0x55, 0x92, 0x42, 0xf0, 0xf4, 0x9f, 0x1d, 0x8b,
 0x5e, 0xd5, 0xa4, 0xd5, 0x75, 0xc9, 0x63, 0x19, 0xdb, 0xc3, 0x9e, 0xdc, 0xd7, 0x60, 0xa4, 0xa5,
 0x4a, 0x20, 0x49, 0x11, 0xc3, 0xdf, 0x7b, 0x34, 0x3b, 0x24, 0x7a, 0x75, 0x53, 0x8f, 0x5f, 0x41,
 0x9a, 0x04, 0x29, 0x01, 0x0a, 0xf4, 0x6a, 0xb9, 0xc6, 0x5d, 0xe0, 0xa5, 0x2d, 0xae, 0xa4, 0xd3,
 0x02, 0x71, 0x80, 0x02, 0x95, 0xdc, 0xbc, 0x5d, 0xe0, 0xa7, 0xe4, 0x46, 0x08, 0x04, 0x7c, 0x70,
 0x38, 0x1c, 0xa8, 0x5b, 0x6b, 0x03, 0x11, 0xd8, 0x82, 0x12, 0xdc, 0x2f, 0xbf, 0x81, 0x90, 0x90,
 0x10, 0x02, 0x8b, 0xc5, 0x82, 0x48, 0x24, 0xf2, 0x89, 0x58, 0x9c, 0x8c, 0xa0, 0x04, 0x15, 0x65,
 0x45, 0x4e, 0x01, 0x97, 0xcb, 0x21, 0xed, 0xf2, 0x45, 0x66, 0x46, 0x5a, 0x70, 0x82, 0xbe, 0xce,
 0x06, 0x08, 0x85, 0xb1, 0x54, 0x72, 0x2e, 0xb4, 0xef, 0x9f, 0xff, 0xff, 0x16, 0xd1, 0xe4, 0x64,
 0x67, 0x90, 0xf2, 0x69, 0x99, 0xeb, 0xf3, 0xe1, 0xde, 0x16, 0x8c, 0x19, 0xda, 0x31, 0x36, 0xd0,
 0x86, 0xc1, 0xee, 0x37, 0xff, 0x2e, 0xe8, 0xed, 0x68, 0x80, 0x41, 0xa3, 0xc0, 0xf7, 0x99, 0x21,
 0x58, 0x2d, 0x26, 0x98, 0x26, 0xbf, 0x60, 0xbe, 0xb3, 0x1b, 0x8b, 0x5a, 0x1d, 0x2c, 0xdf, 0x7e,
 0xc0, 0x6c, 0x32, 0x62, 0x76, 0x42, 0x67, 0x03, 0xb0, 0x3b, 0x28, 0x81, 0x48, 0x94, 0x08, 0x3e,
 0x9f, 0x8f, 0x61, 0x5d, 0x2b, 0xac, 0x6b, 0xab, 0xf8, 0x5a, 0xa7, 0x80, 0x86, 0x93, 0x8a, 0xb6,
 0x03, 0x2c, 0x37, 0x7a, 0x12, 0xf2, 0xf0, 0xeb, 0x9d, 0x8a, 0xca, 0x8f, 0x5e, 0x8a, 0xfd, 0x5e,
 0x05, 0x65, 0x25, 0x97, 0x21, 0xe0, 0x71, 0xf1, 0xaa, 0xbe, 0x0a, 0x39, 0x99, 0x62, 0xb2, 0xc0,
 0x52, 0xa9, 0x14, 0xd6, 0x45, 0x23, 0xf4, 0x59, 0x97, 0x76, 0x24, 0xde, 0xce, 0xc8, 0xad, 0x0a,
 0xd8, 0x36, 0x36, 0xa7, 0x29, 0xc9, 0x3e, 0x8f, 0x82, 0xb3, 0xd9, 0x12, 0x92, 0x34, 0x32, 0x32,
 0x82, 0xfc, 0xf2, 0x79, 0x31, 0x58, 0x5c, 0x98, 0x47, 0x7f, 0xce, 0x15, 0x74, 0x32, 0xe3, 0x31,
 0x71, 0xe7, 0x91, 0x47, 0xe8, 0x0a, 0x1c, 0x92, 0x09, 0x59, 0x35, 0x5d, 0xc9, 0x5b, 0x8f, 0x82,
 0x38, 0x21, 0x8f, 0x24, 0x66, 0x32, 0x99, 0x28, 0xbc, 0x98, 0x8f, 0xe9, 0xf1, 0x3e, 0xcc, 0xbd,
 0x68, 0x21, 0x03, 0x35, 0x31, 0x52, 0x7a, 0x20, 0x69, 0x85, 0x23, 0xf1, 0xe4, 0xbd, 0xc7, 0xe4,
 0xd9, 0xc6, 0xaa, 0x09, 0xba, 0xf4, 0x82, 0x2d, 0xc9, 0x21, 0x36, 0x56, 0x06, 0x47, 0xc1, 0x70,
 0xdd, 0x24, 0x51, 0x51, 0x51, 0xce, 0x6f, 0x9e, 0xe6, 0x6e, 0x69, 0x21, 0x11, 0xae, 0xad, 0x2e,
 0x51, 0xb3, 0x3b, 0xe7, 0x26, 0x18, 0x2d, 0xa9, 0x74, 0xce, 0xb6, 0xe3, 0x98, 0x00, 0x8e, 0xd8,
 0x34, 0x5b, 0x9c, 0x92, 0x4f, 0xd7, 0x64, 0x60, 0xb8, 0x6e, 0x12, 0x36, 0x9b, 0x85, 0xf0, 0xf0,
 0x70, 0x84, 0x85, 0x86, 0x12, 0xc1, 0xed, 0xe2, 0x0b, 0x30, 0x68, 0x15, 0x58, 0x5f, 0x58, 0x42,
 0xdb, 0x41, 0xb6, 0x9b, 0x60, 0xb6, 0xf6, 0x25, 0x59, 0x0f, 0x9a, 0x81, 0xbc, 0x22, 0xb8, 0x06,
 0x5d, 0x11, 0xfd, 0xae, 0x3a, 0x9a, 0xda, 0xd5, 0x9e, 0x5a, 0x24, 0x8c, 0x3d, 0xe9, 0xac, 0xa2,
 0xe8, 0x6a, 0x01, 0x56, 0x86, 0xc6, 0x9c, 0xb3, 0x75, 0x08, 0x7c, 0x85, 0x43, 0x40, 0xb7, 0xc9,
 0xe3, 0x69, 0xea, 0x58, 0x64, 0xd6, 0x89, 0x68, 0x24, 0x25, 0x25, 0xc1, 0x34, 0x3e, 0xb5, 0x43,
 0x40, 0xf7, 0xf7, 0x67, 0x73, 0xbb, 0x1b, 0xf6, 0x4f, 0xd4, 0x29, 0x50, 0x1e, 0x3e, 0x09, 0x8f,
 0xf7, 0xc1, 0x83, 0xf2, 0x9b, 0x48, 0x16, 0xc5, 0x43, 0xd9, 0x5c, 0x83, 0xdc, 0xec, 0x4c, 0x98,
 0x8d, 0x2b, 0x50, 0x1e, 0xe1, 0x79, 0x5d, 0x03, 0x07, 0x5d, 0x11, 0x09, 0x6e, 0x82, 0xee, 0xb8,
 0x5c, 0xf8, 0xbd, 0xd1, 0x14, 0xf5, 0x0f, 0xa1, 0xd7, 0xe9, 0x60, 0x38, 0x7f, 0x3d, 0x68, 0xc1,
 0xe7, 0xca, 0x9a, 0xad, 0x83, 0xaf, 0xbf, 0x53, 0x1e, 0x67, 0xbf, 0x93, 0xd7, 0x3d, 0x49, 0x64,
 0xb2, 0x32, 0x2c, 0x7f, 0x1c, 0x21, 0x3d, 0x0d, 0xb4, 0x45, 0x5d, 0xa1, 0xf1, 0x58, 0x5f, 0x5a,
 0xb6, 0x32, 0x02, 0x0d, 0x6a, 0xdc, 0xeb, 0xe9, 0xea, 0x3a, 0xd2, 0x57, 0x7a, 0x43, 0xf9, 0x42,
 0x1d, 0x7d, 0x06, 0xbf, 0xdb, 0x35, 0xb4, 0xab, 0x24, 0x18, 0xc1, 0x5e, 0x0a, 0xdd, 0xcc, 0x93,
 0x26, 0x22, 0xf1, 0x76, 0x4c, 0x74, 0x85, 0x9d, 0xc6, 0xbc, 0x52, 0x4b, 0x27, 0x97, 0x53, 0xec,
 0x62, 0x04, 0x13, 0xd4, 0x80, 0x3d, 0x14, 0x4f, 0xcd, 0xb3, 0x73, 0xb6, 0xb1, 0xd2, 0x2a, 0x68,
 0x05, 0x59, 0x50, 0x1e, 0xe5, 0xa3, 0xe3, 0xb8, 0x10, 0x3d, 0x89, 0xf9, 0x98, 0xaa, 0x7a, 0x86,
 0x3f, 0xc6, 0x65, 0x0b, 0xf5, 0x4e, 0xb1, 0x23, 0xf9, 0x5f, 0xb0, 0xe5, 0xc8, 0xbc, 0x1f, 0xe6,
 0x36, 0xc7, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE save_symbol_to_schematic_xpm[1] = {{ png, sizeof( png ), "save_symbol_to_schematic_xpm" }};

//EOF
