
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x03, 0x7a, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xad, 0x95, 0xf9, 0x4f, 0x13,
 0x41, 0x14, 0xc7, 0xfb, 0x87, 0xf8, 0xb3, 0x67, 0xe2, 0x7f, 0x60, 0x8c, 0x1a, 0x8f, 0xe0, 0x81,
 0x89, 0x6d, 0x81, 0x16, 0x24, 0x1a, 0x28, 0x28, 0x68, 0x25, 0x9a, 0x08, 0x78, 0xe0, 0x91, 0xa8,
 0x88, 0x67, 0xad, 0x04, 0x11, 0x45, 0x8c, 0x47, 0xa0, 0xad, 0xa0, 0x40, 0xd5, 0x18, 0xb9, 0x0b,
 0x3d, 0x76, 0x29, 0x16, 0x11, 0x44, 0xa2, 0xa5, 0x40, 0xf1, 0xaa, 0xa5, 0x50, 0x84, 0xd2, 0xe3,
 0xeb, 0xec, 0x54, 0x16, 0x4a, 0xa9, 0x45, 0x74, 0x92, 0x6f, 0x76, 0x77, 0x36, 0xef, 0x7d, 0xe6,
 0xbd, 0x37, 0x6f, 0x46, 0x20, 0x20, 0xe3, 0x4d, 0xb3, 0x7a, 0xfb, 0x1b, 0x9d, 0xda, 0xd4, 0xa1,
 0x53, 0x7b, 0x88, 0xb0, 0x08, 0x71, 0x76, 0x46, 0xb3, 0x4e, 0xb3, 0x55, 0x30, 0x77, 0x70, 0xce,
 0x17, 0xe9, 0x74, 0x5e, 0x99, 0x5b, 0xd4, 0x31, 0x21, 0x80, 0x8e, 0x56, 0x0d, 0xf3, 0x3f, 0x01,
 0x1d, 0xad, 0x6a, 0x7d, 0x28, 0x60, 0x4e, 0x5a, 0x0e, 0x1f, 0xda, 0x0f, 0xa1, 0x50, 0x18, 0xa2,
 0xb8, 0x38, 0x31, 0xd2, 0x64, 0x7b, 0xa1, 0xb8, 0x7c, 0x0a, 0x6d, 0xaf, 0x1f, 0x45, 0x83, 0x4c,
 0xcc, 0x05, 0xd0, 0x1f, 0xba, 0x57, 0x0f, 0x50, 0xaf, 0xbd, 0x87, 0xa4, 0x44, 0x09, 0xd2, 0x89,
 0xb3, 0x2b, 0xf9, 0x27, 0x78, 0x9d, 0x3f, 0x93, 0x0d, 0x79, 0x66, 0x1a, 0x85, 0xa5, 0xec, 0x4d,
 0x46, 0x5d, 0x6d, 0xe9, 0x9f, 0x00, 0x81, 0x30, 0xc0, 0xeb, 0xea, 0xbb, 0x10, 0x89, 0x44, 0xfc,
 0x8a, 0x4f, 0xe4, 0x66, 0xcd, 0x6b, 0xac, 0x79, 0x78, 0x03, 0x52, 0x69, 0x02, 0x8d, 0x86, 0x6d,
 0xaa, 0x58, 0x38, 0x80, 0x69, 0x2a, 0x47, 0xb1, 0xf2, 0x1c, 0x0a, 0xaf, 0x9d, 0x45, 0x42, 0x7c,
 0x5c, 0x44, 0x00, 0xa7, 0xeb, 0x05, 0x79, 0x74, 0x11, 0x47, 0xb2, 0x32, 0x70, 0xf3, 0xda, 0x19,
 0x30, 0x8d, 0xe5, 0xd1, 0x01, 0xb3, 0xb5, 0x3b, 0x49, 0x1a, 0x11, 0xf0, 0xb2, 0xaa, 0x04, 0x62,
 0xb1, 0x98, 0xd6, 0x44, 0x22, 0x89, 0x0f, 0x46, 0x9b, 0x93, 0x85, 0xca, 0x47, 0x4a, 0xbc, 0xa8,
 0xbc, 0xfd, 0xef, 0x00, 0x2e, 0x4a, 0xce, 0x69, 0x8d, 0xaa, 0x08, 0x9d, 0xfa, 0xa7, 0x28, 0x2a,
 0x54, 0x40, 0xa9, 0x54, 0x42, 0xa1, 0x50, 0x20, 0x2f, 0xef, 0x24, 0xd8, 0x16, 0xd5, 0xbf, 0x01,
 0xb4, 0x9a, 0x5b, 0x90, 0xcb, 0xe5, 0xd0, 0xeb, 0x74, 0xf0, 0x4d, 0x4d, 0x61, 0xd2, 0x35, 0x0a,
 0x67, 0xf7, 0x07, 0x8c, 0x7d, 0xec, 0x87, 0x77, 0xfc, 0x27, 0x7c, 0x1e, 0x0f, 0xbe, 0x0f, 0xf5,
 0x81, 0x8c, 0x25, 0x21, 0x80, 0xf6, 0x66, 0x15, 0xca, 0x8a, 0x0b, 0x50, 0x5a, 0x94, 0x0f, 0xa9,
 0x24, 0x21, 0x22, 0x60, 0xa0, 0xaf, 0x9d, 0x3a, 0x1e, 0xae, 0xad, 0x03, 0x2b, 0xdc, 0x87, 0xfa,
 0x65, 0xeb, 0x51, 0xbf, 0x74, 0x1d, 0x55, 0xc3, 0x8a, 0x0d, 0x30, 0x27, 0x66, 0xc1, 0xd1, 0xca,
 0xc0, 0xef, 0xf5, 0x4e, 0x10, 0x48, 0x2c, 0x0f, 0x78, 0xf5, 0xec, 0x4e, 0xc8, 0xbe, 0x9f, 0x0f,
 0x40, 0x9d, 0x4f, 0x4e, 0xa2, 0x73, 0xdf, 0x71, 0xde, 0x69, 0x24, 0xf5, 0x1c, 0xbf, 0x04, 0xf8,
 0xfd, 0x5e, 0x0a, 0x99, 0x76, 0xd0, 0xf4, 0xe2, 0x7e, 0xc4, 0x3e, 0x28, 0xbb, 0xa3, 0xa4, 0x2b,
 0xb7, 0xc8, 0x72, 0xc0, 0x4a, 0xe5, 0x30, 0xa7, 0x1c, 0x85, 0x69, 0x57, 0x3a, 0x6c, 0x6a, 0x2d,
 0x1c, 0x5d, 0xef, 0xe1, 0xe8, 0xee, 0xa5, 0xef, 0xc6, 0xd8, 0xd4, 0x19, 0x48, 0x6e, 0x01, 0x02,
 0x3e, 0xdf, 0x44, 0x58, 0x0d, 0x0e, 0x66, 0xca, 0xc2, 0x3a, 0xd9, 0x6c, 0x32, 0x61, 0x48, 0xa3,
 0xa5, 0x86, 0x5f, 0x0c, 0x66, 0xb8, 0x9d, 0x23, 0x70, 0x8f, 0x8e, 0x62, 0x7c, 0x7c, 0x1c, 0x23,
 0xb6, 0x41, 0xa2, 0x21, 0xfa, 0xee, 0x1e, 0x73, 0xa3, 0x2b, 0x27, 0x9f, 0x87, 0x38, 0x75, 0xec,
 0x64, 0x18, 0xa0, 0xbd, 0xb9, 0x02, 0xfa, 0xba, 0xc7, 0xbc, 0xd8, 0x96, 0x2a, 0x04, 0xfc, 0x7e,
 0x18, 0xb6, 0x24, 0x53, 0x23, 0xfb, 0xcb, 0x46, 0xea, 0x6c, 0x8c, 0x40, 0x86, 0x1b, 0xda, 0xf8,
 0x3a, 0x30, 0xf1, 0x99, 0x18, 0xe9, 0x1f, 0x20, 0xe0, 0x31, 0xe8, 0x63, 0xf6, 0xd0, 0x39, 0x73,
 0xe2, 0x21, 0x08, 0xa2, 0x1d, 0x5e, 0xd6, 0xf7, 0x7a, 0x4c, 0xd8, 0x3f, 0xf3, 0xab, 0xb2, 0x3e,
 0xac, 0xa2, 0x00, 0x4e, 0xdf, 0x58, 0x0b, 0x2d, 0xee, 0xf4, 0x3f, 0x46, 0x72, 0x90, 0xce, 0x7f,
 0x2a, 0x53, 0xf3, 0x85, 0x8f, 0x0a, 0xb0, 0x5b, 0x2d, 0x70, 0xb4, 0xb1, 0x21, 0x45, 0xd4, 0xad,
 0x11, 0xa1, 0x61, 0xe5, 0xc6, 0x79, 0x0b, 0x6c, 0x91, 0x9f, 0x46, 0xdb, 0xe6, 0xdd, 0xfc, 0xb7,
 0x20, 0xda, 0x25, 0x63, 0xb7, 0x76, 0x86, 0x00, 0x7a, 0xaf, 0x96, 0xc0, 0xf6, 0xe4, 0x79, 0x98,
 0xe3, 0xc6, 0x55, 0x1b, 0xd1, 0xba, 0x36, 0x0e, 0x8c, 0x38, 0x23, 0x04, 0x1e, 0xf5, 0x3e, 0x98,
 0x9b, 0x22, 0x9b, 0xaa, 0x86, 0xa6, 0x81, 0x25, 0xe9, 0x98, 0x0d, 0x70, 0xf4, 0xf4, 0xf1, 0xa9,
 0x1b, 0xd4, 0xd6, 0x05, 0xe7, 0x97, 0xaf, 0xe7, 0x00, 0xaa, 0x6d, 0x7f, 0x02, 0x74, 0x99, 0x6a,
 0x80, 0x40, 0x00, 0x86, 0x4d, 0x49, 0xd4, 0xc8, 0xb8, 0x53, 0x46, 0x0b, 0xe9, 0xb4, 0x0e, 0x80,
 0x49, 0x98, 0x81, 0xf4, 0x97, 0x57, 0xc3, 0xfd, 0xc3, 0x49, 0x01, 0x9f, 0x5b, 0x4c, 0xc1, 0x22,
 0x93, 0x2d, 0xfd, 0xfb, 0x56, 0xe3, 0x20, 0x1a, 0x43, 0xa4, 0x74, 0xb9, 0xbe, 0x0e, 0xc2, 0xae,
 0x99, 0x49, 0x4b, 0x57, 0xee, 0x45, 0x0a, 0xe1, 0x9c, 0xb9, 0x06, 0xed, 0x74, 0xab, 0xd2, 0x6d,
 0xea, 0x72, 0x61, 0x84, 0x7c, 0x3b, 0xde, 0xf6, 0x04, 0xa3, 0x6a, 0x32, 0x7a, 0x04, 0x0b, 0x19,
 0xa4, 0x23, 0x57, 0x07, 0x7c, 0x7e, 0x9f, 0x25, 0x35, 0x9b, 0x87, 0x18, 0x76, 0xa4, 0xc0, 0x56,
 0x51, 0x03, 0xc7, 0xbb, 0x5e, 0xda, 0x6c, 0xfd, 0x8f, 0x9f, 0xd2, 0xe8, 0x8c, 0xb1, 0x29, 0xf4,
 0xd9, 0x7d, 0xf4, 0x42, 0xb0, 0xd1, 0x16, 0x3a, 0x48, 0xeb, 0x67, 0xf8, 0x3d, 0x53, 0xde, 0xce,
 0xf4, 0x63, 0x51, 0x8f, 0x8a, 0xee, 0xec, 0x7c, 0xce, 0xb9, 0x8f, 0x2c, 0x4c, 0x28, 0xf8, 0x9b,
 0x41, 0x0c, 0x64, 0x24, 0x12, 0xef, 0xd7, 0xe7, 0x0d, 0x53, 0x0c, 0x39, 0x2a, 0x66, 0x1f, 0x76,
 0x5c, 0x41, 0xdb, 0xe3, 0x0f, 0xd0, 0xb4, 0x04, 0x82, 0x87, 0x9d, 0x88, 0xb3, 0xf9, 0x05, 0x69,
 0xea, 0xed, 0x7c, 0xb0, 0xad, 0x12, 0x30, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae,
 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE update_lib_symbols_in_schematic_xpm[1] = {{ png, sizeof( png ), "update_lib_symbols_in_schematic_xpm" }};

//EOF
