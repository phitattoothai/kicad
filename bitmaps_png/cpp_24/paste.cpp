
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x04, 0x00, 0x00, 0x00, 0x4a, 0x7e, 0xf5,
 0x73, 0x00, 0x00, 0x00, 0xa1, 0x49, 0x44, 0x41, 0x54, 0x38, 0xcb, 0x63, 0x60, 0xa0, 0x06, 0x08,
 0xcd, 0x0f, 0xf9, 0x1f, 0xf2, 0x3f, 0x38, 0x8f, 0x28, 0xc5, 0x09, 0x2b, 0xc2, 0x7f, 0x81, 0x94,
 0x83, 0xe1, 0xbf, 0x84, 0x35, 0x84, 0x4d, 0xff, 0xfb, 0xfe, 0xff, 0x67, 0x28, 0xfc, 0xf0, 0x3f,
 0xf4, 0x0f, 0x0e, 0x65, 0x11, 0xb2, 0x21, 0xdb, 0x83, 0x3f, 0x81, 0x4c, 0x0d, 0xfd, 0xff, 0x09,
 0xae, 0xe1, 0xed, 0xff, 0x08, 0x90, 0x3d, 0x7f, 0x42, 0xde, 0x84, 0xce, 0x0c, 0x14, 0x46, 0xd1,
 0x10, 0x72, 0x0c, 0x28, 0x0c, 0x76, 0x46, 0xcc, 0x7f, 0xb8, 0x83, 0x80, 0x9a, 0xa3, 0x61, 0xec,
 0x1f, 0x21, 0x7b, 0x50, 0x34, 0x04, 0xff, 0x08, 0xf9, 0x4f, 0x00, 0xfe, 0x46, 0xb5, 0xe1, 0x3f,
 0x61, 0x38, 0xaa, 0x61, 0x54, 0x03, 0x51, 0x1a, 0x7e, 0x93, 0x9a, 0xf8, 0x4e, 0x86, 0xfe, 0xc5,
 0xab, 0xfc, 0x4f, 0xe8, 0x29, 0xd4, 0x7c, 0xa6, 0x15, 0xf2, 0x15, 0x8f, 0x96, 0x3f, 0x21, 0xdf,
 0x42, 0x75, 0xd1, 0xf2, 0x5c, 0xb0, 0x6a, 0xf0, 0xbe, 0x10, 0xec, 0xb9, 0xe2, 0x73, 0xc8, 0x9e,
 0x70, 0x4d, 0xb2, 0x4b, 0x14, 0x00, 0xf8, 0x4b, 0xda, 0xe2, 0xb4, 0x68, 0x26, 0xe8, 0x00, 0x00,
 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE paste_xpm[1] = {{ png, sizeof( png ), "paste_xpm" }};

//EOF
