
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x01, 0x0b, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0x18, 0x48, 0x20,
 0xd3, 0x70, 0xd8, 0x01, 0x84, 0x61, 0x6c, 0xa9, 0xc6, 0xc3, 0x0d, 0xc4, 0xf2, 0x89, 0x02, 0x20,
 0x0d, 0xd2, 0x8d, 0x47, 0x0f, 0xc0, 0x34, 0x4b, 0x35, 0x1e, 0xf9, 0x0f, 0xa2, 0x61, 0x72, 0xf8,
 0xf8, 0x44, 0x5b, 0x00, 0xd3, 0x44, 0xac, 0x0f, 0x60, 0x98, 0x28, 0x9f, 0x90, 0xe3, 0x6d, 0xb2,
 0x7c, 0x42, 0x6a, 0xbc, 0xd1, 0xd4, 0x07, 0x24, 0x01, 0x72, 0xbc, 0x4b, 0x55, 0x1f, 0xfc, 0xff,
 0xff, 0x3f, 0x06, 0x88, 0xf7, 0x01, 0xf1, 0x61, 0x20, 0x2e, 0x01, 0x62, 0x46, 0xaa, 0xc5, 0x01,
 0xd0, 0xb0, 0xe4, 0x93, 0x27, 0x4f, 0xfe, 0x4f, 0x49, 0x49, 0xf9, 0x9b, 0x90, 0x90, 0xf0, 0x77,
 0xe3, 0xc6, 0x8d, 0x40, 0xa1, 0xff, 0x8f, 0xbf, 0xfd, 0xfc, 0xfb, 0xec, 0xfd, 0xb7, 0xdf, 0xef,
 0xde, 0x7c, 0xf9, 0x75, 0x16, 0xc8, 0x17, 0x26, 0xdb, 0x07, 0xff, 0xfe, 0xfd, 0x3b, 0x19, 0x17,
 0x17, 0xf7, 0x37, 0x24, 0x24, 0xe4, 0x3f, 0x36, 0xbc, 0x73, 0xe7, 0x4e, 0x90, 0x85, 0x0e, 0x78,
 0x73, 0x2c, 0x3e, 0xef, 0x02, 0x2d, 0x38, 0x01, 0x72, 0x39, 0x01, 0x0b, 0xec, 0xf1, 0xe6, 0x58,
 0x10, 0x1f, 0x24, 0x8e, 0xcd, 0x02, 0xa0, 0xe6, 0xb8, 0xb3, 0x67, 0xcf, 0xfe, 0xcf, 0xcc, 0xcc,
 0xfc, 0x0b, 0x0a, 0xa6, 0x6d, 0xdb, 0xb6, 0x81, 0x0c, 0x7c, 0x04, 0x0a, 0x9e, 0x07, 0xef, 0xbe,
 0xff, 0x7f, 0xf8, 0xee, 0xfb, 0x23, 0x20, 0x9f, 0x97, 0xa8, 0x1c, 0x8b, 0x27, 0x92, 0x43, 0x80,
 0x78, 0x07, 0x10, 0xef, 0x06, 0xe2, 0x5c, 0x50, 0x24, 0x63, 0x0d, 0x56, 0x9a, 0xa7, 0x77, 0x9a,
 0xe7, 0x64, 0x6a, 0xfa, 0x00, 0x6b, 0xc2, 0xa0, 0x66, 0x01, 0x35, 0x42, 0xe2, 0x80, 0x12, 0x3e,
 0x51, 0x71, 0x40, 0x09, 0x1f, 0x6b, 0xe6, 0xa4, 0xa6, 0x0f, 0x08, 0x65, 0x4e, 0xaa, 0x03, 0x00,
 0x2c, 0x6a, 0xc6, 0xd6, 0xe3, 0xf3, 0x99, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44,
 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE show_zone_disable_xpm[1] = {{ png, sizeof( png ), "show_zone_disable_xpm" }};

//EOF
